<?php

namespace App\Http\Controllers;
use App\Models\Posts;
use App\Models\Category;
use App\Models\Tags;
use Illuminate\Support\Facades\Auth;
use illuminate\Support\Str;

use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Posts $post)
    {
        $post = $post->latest()->paginate(6);
        return view('post.index', compact('post'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        $tags = Tags::all();
        return view('post.create', compact('category','tags'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request,[
            'judul' => 'required',
            'category_id' => 'required',
            'content' => 'required',
            'gambar' => 'required',
            'status' => 'sometimes|boolean',
        ]);

        $gambar = $request->gambar;
        $new_gambar = time().$gambar->getClientOriginalName();
        if ($request->status == null) {
            $request->status = '0';
        }
        

        $post = Posts::create([
            'judul'=> $request->judul,
            'category_id'=> $request->category_id,
            'content'=> $request->content,
            'gambar'=> 'public/uploads/posts/'.$new_gambar,
            'slug'=> Str::slug($request->judul),
            'user_id' => Auth::id(),
            'status' => $request->status,
        ]);
        
        $post->tags()->attach($request->tags);

        $gambar->move('public/uploads/posts/',$new_gambar);
        return redirect()->route('post.index')->with('status','Artikel Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::all();
        $tags = Tags::all();
        $post = Posts::findorfail($id);
        return view('post.edit', compact('post','tags','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'judul' => 'required',
            'category_id' => 'required',
            'content' => 'required',
            'status' => 'sometimes|boolean',
        ]);

        $post = Posts::findorfail($id);

        if($request->has('gambar')) {
            $gambar = $request->gambar;
            $new_gambar = time().$gambar->getClientOriginalName();
            $gambar->move('public/uploads/posts/',$new_gambar);

            $post_data = [
                'judul'=> $request->judul,
                'category_id'=> $request->category_id,
                'content'=> $request->content,
                'gambar'=> 'public/uploads/posts/'.$new_gambar,
                'slug'=> Str::slug($request->judul),
                'status' => $request->status,
                
            ];
        } else{
            $post_data = [
                'judul'=> $request->judul,
                'category_id'=> $request->category_id,
                'content'=> $request->content,
                'slug'=> Str::slug($request->judul),
                'status' => $request->status,
            ];
        }
        
        $post->tags()->sync($request->tags);
        $post->update($post_data);

        return redirect()->route('post.index')->with('status','Artikel Berhasil DiUpdate');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Posts::findorfail($id);
        $post->delete();
        return redirect()->back()->with('status','Artikel Berhasil Dihapus (Trashed Artikel)');
    }
    public function trashed()
    {
        $post = Posts::onlyTrashed()->paginate(8);
        return view('post.hapus', compact('post'));

    }
    public function restore($id)
    {
        $post = Posts::withTrashed()->where('id',$id)->first();
        $post->restore();
        return redirect()->back()->with('status','Artikel Berhasil Direstore (check list artikel) ');

    }
    public function kill($id)
    {
        $post = Posts::withTrashed()->where('id',$id)->first();
        $post->forceDelete();
        return redirect()->back()->with('status','Artikel Berhasil Permanen Hapus');

    }
}
