<?php

namespace App\Http\Controllers;
use App\Models\Karyawan;
use illuminate\Support\Str;

use Illuminate\Http\Request;

class KaryawanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Karyawan $karyawan )
    {
        $karyawan = $karyawan->latest()->paginate(8);
        return view('karyawan.index', compact('karyawan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('karyawan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'judul' => 'required|min:3',
            'category' => 'required',
            'content' => 'required',
        ]);
        $karyawan = Karyawan::create([
            'judul'=> $request->judul,
            'category'=> $request->category,
            'content'=> $request->content,

        ]);
        return redirect()->route('karyawan.index')->with('status','Informasi Berhasil Ditambahkan');
    }
    public function image_upload(Request $request)
    { 
        if ($request->hasFile('upload')){
        $originName = $request->file('upload')-> getClientOriginalName();
        $fileName = pathinfo($originName,PATHINFO_FILENAME);
        $extension = $request->file('upload')-> getClientOriginalExtension();
        $fileName = $fileName.'_'.time().'.'.$extension;

        $request->file('upload')->move(public_path('media'),$fileName);

        $url = asset('media/'.$fileName);
        return response()->json([
            'fileName' => $fileName, 'uploaded'=>1, 'url'=>$url
        ]);
        }

       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $karyawan = Karyawan::findorfail($id);
        return view('karyawan.show', compact('karyawan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $karyawan = Karyawan::findorfail($id);
        return view('karyawan.edit', compact('karyawan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        $this->validate($request,[
            'judul' => 'required|min:3',
            'category' => 'required',
            'content' => 'required',
        ]);
        $karyawan = Karyawan::findorfail($id);
        // dd($karyawan);
        $karyawan_data = [
            'judul'=> $request->judul,
            'category'=> $request->category,
            'content'=> $request->content,
        ];

        $karyawan->update($karyawan_data);

        return redirect()->route('karyawan.index')->with('status','Informasi Berhasil di Update');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $karyawan = Karyawan::findorfail($id);
        $karyawan->delete();

        return redirect()->back()->with('status','Kategori Berhasil Dihapus');
    }
}
