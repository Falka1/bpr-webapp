<?php

namespace App\Http\Controllers;
use App\Models\Posts;
use App\Models\Category;
use illuminate\Support\Str;

use Illuminate\Http\Request;


class BlogController extends Controller
{
    public function index(Posts $post)
    {
        $data = $post->latest()->where('status', 1)->take(5)->get() ;
        return view('front.index', compact('data'));
    }
    public function berita(Posts $post)
    {
        $category_list = Category::all();
        $data_top = $post->latest()->where('status', 1)->take(3)->get() ;
        $data = $post->latest()->where('status', 1)->paginate(6) ;
        return view('front.page1', compact('data_top','data', 'category_list'));
    }
    public function isi_berita($slug)
    {
        $category_list = Category::all();
        $data = Posts::where('slug',$slug)->get() ;
        return view('front.page2', compact('data', 'category_list'));
    }
    public function category_berita(category $category)
    {
        $data = $category->posts()->latest()->paginate(10);
        return view('front.search', compact('data'));
    }
    public function search_berita(Request $request)
    {
        $data = Posts::where('judul',$request->cari)->orWhere('judul','like','%'.$request->cari.'%')->paginate(10);
        return view('front.search', compact('data'));
    }
}
