@extends('master')

@section('sub-judul','Edit Karyawan')
@section('content')
 
 
 <!-- Content Header (Page header) -->
 

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card card-warning">
        <div class="card-header">
            <h3 class="card-title">Form Edit Karyawan</h3>

            <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
            </div>
        </div>
        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success">
                    {{session('status')}}
                </div>
            @endif
            <form class="form-horizontal" role="form" action="{{route('karyawan.update', $karyawan->id)}}" method='post' enctype="multipart/form-data">
            @csrf
            @method('patch')
                    <div class="card-body">
                        <div class="form-group">
                            <label for="judul" class=" col-form-label">Judul</label>
                        <div >
                            <input type="text" class="form-control @error('judul') is-invalid @enderror" id="judul" name="judul" value='{{$karyawan->judul}}' >
                            @error('judul')<div class="invalid-feedback">{{$message}}</div> @enderror
                        </div>
                        </div>
                        <div class="form-group">
                            <label for="category" class=" col-form-label">Pilih Kategori</label>
                        <div >
                            <select class="form-control" id="category" name='category'>
                                <option value="" holder>Pilih Kategori</option>
                                <option value="Job Desc"  @if($karyawan->category == "Job Desc") selected @endif >Job Desc</option>
                                <option value="SOP" @if($karyawan->category == "SOP") selected @endif>SOP</option>
                                <option value="Laporan" @if($karyawan->category == "Laporan") selected @endif>Laporan</option>
                            </select>
                        </div>
                        </div>
                        <div class="form-group">
                            <label for="content" class=" col-form-label">Isi Konten</label>
                        <div >
                            <textarea type="text" class="form-control" id="content" name="content" >{!!$karyawan->content!!}</textarea>
                        </div>
                        </div>

                       
                    </div>
            </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <a type="button" class="btn btn-secondary" href="{{route('karyawan.index')}}">Tutup</a>
            <button type="submit" class="btn btn-success">Tambah</button>
            </form> 
        </div>
        <!-- /.card-footer-->
        </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->

    <script src="https://cdn.ckeditor.com/ckeditor5/34.1.0/classic/ckeditor.js"></script>
    <script>
       ClassicEditor
            .create( document.querySelector( '#content' ), {
               ckfinder:{
                    uploadUrl: '{{route("karyawan.image_upload")."?_token=".csrf_token()}}',  
                },
                // ...
            } )                                                                     
            .catch( error => {
                console.error( error );
            } );
    </script>

    @endsection
