@extends('master')

@section('sub-judul','Office Info')
@section('content')
 
 
 <!-- Content Header (Page header) -->
 

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title"> {{$karyawan->category}}</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
        <h1 > {{$karyawan->judul}}</h1><br>
        {!! $karyawan -> content !!}
                
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
        <a type="button" class="btn btn-secondary" href="{{route('karyawan.index')}}">Tutup</a>
        </div>
        <!-- /.card-footer-->
      </div>

      <!-- /.card -->

    </section>
    <!-- /.content -->



    @endsection
