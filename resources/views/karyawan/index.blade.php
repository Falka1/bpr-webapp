@extends('master')

@section('sub-judul','Informasi')
@section('content')
 
 
 <!-- Content Header (Page header) -->
 

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Office Information</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
        @if (session('status'))
          <div class="alert alert-success">
              {{session('status')}}
          </div>
        @endif

            <!-- Button trigger modal -->
            @if(auth()->user()->role == 1)
            <a type="button" class="btn btn-primary" href="{{route('karyawan.create')}}">Tambah Artikel</a>
            @endif
                <table id="datasantri" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Judul</th>
                    <th>Kategori</th>
                    <th width="35%">Action</th>
                </tr>
                </thead>
                <tbody>
                
                @foreach($karyawan as $result => $hasil)
                <tr>
                    <td>{{$result + $karyawan->firstitem()}}</td>
                    <td> {{$hasil->judul}} </td>
                    <td> {{$hasil->category}} </td>
                    <td >
                        <form action="{{route('karyawan.destroy', $hasil->id)}}" method="POST">
                          @csrf 
                          @method('delete')
                          <a type="button" class="btn btn-primary" href="{{route('karyawan.show', $hasil->id)}}">Lihat</a>
                        <a type="button" class="btn btn-warning" href="{{route('karyawan.edit', $hasil->id)}}">Ubah</a>
                        @if(auth()->user()->role == 1)
                        <button type="submit" class="btn btn-danger" onclick="return confirm('data akan dihapus...')" >Hapus</button>
                        @endif  
                      </form>
                    </td>
                </tr>
                @endforeach
                </tbody>
                </table>
                
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
        {{$karyawan->links('pagination::bootstrap-4')}}
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->



    @endsection
