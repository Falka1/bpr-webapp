@extends('master')

@section('sub-judul','Tambah informasi')
@section('content')
 
 
 <!-- Content Header (Page header) -->
 

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
            <h3 class="card-title">Form Artikel Office</h3>

            <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
            </div>
        </div>
        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success">
                    {{session('status')}}
                </div>
            @endif
            <form class="form-horizontal" role="form" action="{{route('karyawan.store')}}" method='post' enctype="multipart/form-data">
            @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="judul" class=" col-form-label">Judul</label>
                        <div >
                            <input type="text" class="form-control @error('judul') is-invalid @enderror" id="judul" name="judul" >
                            @error('judul')<div class="invalid-feedback">{{$message}}</div> @enderror
                        </div>
                        </div>
                        <div class="form-group">
                            <label for="category" class=" col-form-label">Pilih Kategori</label>
                        <div >
                            <select class="form-control" id="category" name='category'>
                                <option value="" holder>Pilih Kategori</option>
                                <option value="Job Desc">Job Desc</option>
                                <option value="SOP">SOP</option>
                                <option value="Laporan">Laporan</option>
                            </select>
                        </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="content" class=" col-form-label">Isi Konten</label>
                        <div >
                            <textarea type="text"  class="form-control @error('content') is-invalid @enderror" id="content" name="content" ></textarea>
                            @error('content')<div class="invalid-feedback">{{$message}}</div> @enderror
                        </div>
                        </div>
                    </div>
            </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <a type="button" class="btn btn-secondary" href="{{route('karyawan.index')}}">Tutup</a>
            <button type="submit" class="btn btn-success">Tambah</button>
            </form> 
        </div>
        <!-- /.card-footer-->
        </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->

    <script src="https://cdn.ckeditor.com/ckeditor5/34.1.0/classic/ckeditor.js"></script>
    <script>


        ClassicEditor
            .create( document.querySelector( '#content' ), {
               ckfinder:{
                    uploadUrl: '{{route("karyawan.image_upload")."?_token=".csrf_token()}}',  
                },
                
                // ...
            } )                                                                     
            .catch( error => {
                console.error( error );
            } );
    </script>

    @endsection
