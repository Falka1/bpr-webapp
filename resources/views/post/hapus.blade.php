@extends('master')

@section('sub-judul','Trashed Artikel')
@section('content')
 
 
 <!-- Content Header (Page header) -->
 

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Tabel Artikel</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
        @if (session('status'))
          <div class="alert alert-success">
              {{session('status')}}
          </div>
        @endif

            <!-- Button trigger modal -->
            <div class="table-responsive">
                <table id="dataartikel" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Judul Artikel</th>
                    <th>Kategori</th>
                    <th>Tags</th>
                    <th>Gambar</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                
                @foreach($post as $result => $hasil)
                <tr>
                    <td>{{$result + $post->firstitem()}}</td>
                    <td> {{$hasil->judul}} </td>
                    <td> {{$hasil->category->name}} </td>
                    <td> 
                        @foreach($hasil->tags as $tag)
                        <span class=" badge badge-primary">{{$tag->name}}</span>
                        @endforeach                      
                    </td>
                    <td><img src="{{asset($hasil->gambar)}}" class="img-fluid" style="width:100px"></td>
                    <td >
                        <form action="{{route('post.kill', $hasil->id)}}" method="POST">
                          @csrf 
                          @method('delete')
                        <a type="button" class="btn btn-warning" href="{{route('post.restore', $hasil->id)}}">Restore</a>
                        <button type="submit" class="btn btn-danger" onclick="return confirm('data akan dihapus...')">Hapus</button>
                        </form>
                    </td>
                </tr>
                @endforeach
                </tbody>
                </table>
              </div>
                
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
        {{$post->links('pagination::bootstrap-4')}}
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->

    @endsection
