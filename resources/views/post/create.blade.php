@extends('master')

@section('sub-judul','Tambah Artikel')
@section('content')
 
 
 <!-- Content Header (Page header) -->
 

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
            <h3 class="card-title">Form Artikel</h3>

            <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
            </div>
        </div>
        <div class="card-body">
            @if (session('status'))
                <div class="alert alert-success">
                    {{session('status')}}
                </div>
            @endif
            <form class="form-horizontal" role="form" action="{{route('post.store')}}" method='post' enctype="multipart/form-data">
            @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="judul" class=" col-form-label">Judul</label>
                        <div >
                            <input type="text" class="form-control @error('judul') is-invalid @enderror" id="judul" name="judul" >
                            @error('judul')<div class="invalid-feedback">{{$message}}</div> @enderror
                        </div>
                        </div>
                        <div class="form-group">
                            <label for="category_id" class=" col-form-label">Pilih Kategori</label>
                        <div >
                            <select class="form-control" id="category_id" name='category_id'>
                                <option value="" holder>Pilih Kategori</option>
                                @foreach($category as $result)
                                <option value="{{$result->id}}">{{$result->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        </div>
                        <div class="form-group">
                            <label>Tags</label>
                            <select class="select2" id='select2' multiple="multiple" data-placeholder="Pilih Tags" style="width: 100%;" name='tags[]'>
                                @foreach($tags as $tag)
                                <option value="{{$tag->id}}">{{$tag->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="content" class=" col-form-label">Isi Konten</label>
                        <div >
                            <textarea type="text"  class="form-control @error('content') is-invalid @enderror" id="content" name="content" ></textarea>
                            @error('content')<div class="invalid-feedback">{{$message}}</div> @enderror
                        </div>
                        </div>
                        @if(auth()->user()->role == 1)
                        <div class="form-group">
                            <div class="row">
                            <legend class="col-form-label col-sm-2 pt-0">Verifikasi Post</legend>
                            <div class="col-sm-10">
                                <div class="form-check">
                                <input class="form-check-input" type="radio" name="status" id="status_aktif" value="1" >
                                <label class="form-check-label" for="status_aktif">
                                    Aktif
                                </label>
                                </div>
                                <div class="form-check">
                                <input class="form-check-input" type="radio" name="status" id="status_tidak" value="0" checked>
                                <label class="form-check-label" for="status_tidak">
                                    Tidak Aktif
                                </label>
                                </div>
                            </div>
                            </div>
                        </div>
                        @endif

                        <div class="form-group">
                            <label for="gambar" class=" col-form-label">Thumbnail</label>
                        <div >
                            <input type="file" class="form-control @error('gambar') is-invalid @enderror" id="gambar" name="gambar" >
                            @error('gambar')<div class="invalid-feedback">{{$message}}</div> @enderror
                        </div>
                        </div>
                    </div>
            </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <a type="button" class="btn btn-secondary" href="{{route('post.index')}}">Tutup</a>
            <button type="submit" class="btn btn-success">Tambah</button>
            </form> 
        </div>
        <!-- /.card-footer-->
        </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->

    <script src="https://cdn.ckeditor.com/ckeditor5/34.1.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create( document.querySelector( '#content' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>

    @endsection
