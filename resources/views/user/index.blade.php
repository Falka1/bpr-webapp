@extends('master')

@section('sub-judul','Pengaturan User')
@section('content')
 
 
 <!-- Content Header (Page header) -->
 

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Tabel User</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
        @if (session('status'))
          <div class="alert alert-success">
              {{session('status')}}
          </div>
        @endif

            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
            Tambah User
            </button>
                <table id="datasantri" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Nama User</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                
                @foreach($user as $result => $hasil)
                <tr>
                    <td>{{$result + $user->firstitem()}}</td>
                    <td> {{$hasil->name}} </td>
                    <td> {{$hasil->email}} </td>
                    <td> 
                         @if($hasil->role == 1)
                         <span class=" badge badge-warning">Administrator </span>
                         @elseif($hasil->role == 2)   
                         <span class=" badge badge-success">Author  </span>
                         @else
                         <span class=" badge badge-info">User </span>
                         @endif
                    </td>
                    <td >
                        <form action="{{route('user.destroy', $hasil->id)}}" method="POST">
                          @csrf 
                          @method('delete')
                        <a type="button" class="btn btn-warning" href="{{route('user.edit', $hasil->id)}}">Ubah</a>
                        <button type="submit" class="btn btn-danger" onclick="return confirm('data akan dihapus...')" >Hapus</button>
                        </form>
                    </td>
                </tr>
                @endforeach
                </tbody>
                </table>
                
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
        {{$user->links('pagination::bootstrap-4')}}
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->


<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Tambah User</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <form class="form-horizontal" role="form" action="{{route('user.store')}}" method='POST' enctype="multipart/form-data">
      @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="name" class=" col-form-label">Nama User</label>
                <div >
                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="Tulis disini....">
                    @error('name')<div class="invalid-feedback">{{$message}}</div> @enderror
                </div>
                <div class="form-group">
                    <label for="email" class=" col-form-label">Email</label>
                <div >
                    <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="Tulis disini....">
                    @error('email')<div class="invalid-feedback">{{$message}}</div> @enderror
                </div>
                <div class="form-group">
                            <label for="role" class=" col-form-label">Pilih Role</label>
                        <div >
                            <select class="form-control" id="role" name='role'>
                                <option value="" holder>Pilih Role</option>
                                <option value="1">Administrator</option>
                                <option value="2">Author</option>
                                <option value="3">User</option>
                            </select>
                        </div>
                        </div>
                <div class="form-group">
                    <label for="password" class=" col-form-label">Password</label>
                <div >
                    <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" placeholder="Tulis disini....">
                    @error('password')<div class="invalid-feedback">{{$message}}</div> @enderror
                </div>
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-success">Simpan</button>
        </form> 
      </div>
    </div>
  </div>
</div>
    @endsection
