@extends('master')

@section('sub-judul','Edit User')
@section('content')


<section class="content">

<!-- Default box -->
<div class="card card-warning">
  <div class="card-header">
    <h3 class="card-title">Edit User</h3>

    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
        <i class="fas fa-minus"></i>
      </button>
      <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
        <i class="fas fa-times"></i>
      </button>
    </div>
  </div>
  <div class="card-body">
    @if (session('status'))
        <div class="alert alert-success">
            {{session('status')}}
        </div>
    @endif
    <form class="form-horizontal" role="form" action="{{route('user.update', $user->id)}}" method='POST' enctype="multipart/form-data">
      @csrf
      @method('patch')
            <div class="card-body">
            <div class="form-group">
                    <label for="name" class=" col-form-label">Nama User</label>
                <div >
                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{$user->name }}">
                    @error('name')<div class="invalid-feedback">{{$message}}</div> @enderror
                </div>
                <div class="form-group">
                    <label for="email" class=" col-form-label">Email</label>
                <div >
                    <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{$user->email }}">
                    @error('email')<div class="invalid-feedback">{{$message}}</div> @enderror
                </div>
                <div class="form-group">
                            <label for="role" class=" col-form-label">Pilih Role</label>
                        <div >
                            <select class="form-control" id="role" name='role'>
                                <option value="" holder>Pilih Role</option>
                                <option value="1" @if( $user->role == '1') selected @endif>Administrator</option>
                                <option value="2" @if( $user->role == '2') selected @endif>Author</option>
                                <option value="3" @if( $user->role == '3') selected @endif>User</option>
                            </select>
                        </div>
                        </div>
                <div class="form-group">
                    <label for="password" class=" col-form-label">Password</label>
                <div >
                    <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" placeholder="Tulis disini....">
                    @error('password')<div class="invalid-feedback">{{$message}}</div> @enderror
                </div>
                </div>
            </div>
    </div>
  <!-- /.card-body -->
  <div class="card-footer">
    <a type="button" class="btn btn-secondary" href="{{route('user.index')}}">Tutup</a>
    <button type="submit" class="btn btn-success">Update</button>
    </form> 
  </div>
  <!-- /.card-footer-->
</div>
<!-- /.card -->

</section>

@endsection