@extends('master')

@section('sub-judul','Kategori')
@section('content')
 
 
 <!-- Content Header (Page header) -->
 

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Tabel Kategori</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
        @if (session('status'))
          <div class="alert alert-success">
              {{session('status')}}
          </div>
        @endif

            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
            Tambah Kategori
            </button>
                <table id="datasantri" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Kategori</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                
                @foreach($category as $result => $hasil)
                <tr>
                    <td>{{$result + $category->firstitem()}}</td>
                    <td> {{$hasil->name}} </td>
                    <td >
                        <form action="{{route('category.destroy', $hasil->id)}}" method="POST">
                          @csrf 
                          @method('delete')
                        <a type="button" class="btn btn-warning" href="{{route('category.edit', $hasil->id)}}">Ubah</a>
                        <button type="submit" class="btn btn-danger" onclick="return confirm('data akan dihapus...')" >Hapus</button>
                        </form>
                    </td>
                </tr>
                @endforeach
                </tbody>
                </table>
                
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
        {{$category->links('pagination::bootstrap-4')}}
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->


<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Tambah Kategori</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      <form class="form-horizontal" role="form" action="{{route('category.store')}}" method='POST' enctype="multipart/form-data">
      @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="name" class=" col-form-label">Nama Kategori</label>
                <div >
                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="Tulis disini....">
                    @error('name')<div class="invalid-feedback">{{$message}}</div> @enderror
                </div>
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-success">Simpan</button>
        </form> 
      </div>
    </div>
  </div>
</div>
    @endsection
