@extends('front.master')

@section('front')

			<section class="content1">
				<div class="container">
					<h2 class="text-primary fw-bold mt-3">Hasil Pencarian Terkait</h2>
					@foreach($data as $hasil)
					<div class="search mt-4">
						<h4 class="fw-bold">{{$hasil->judul}}</h4>
						<p class="col-8">
						{!! \Illuminate\Support\Str::limit($hasil -> content,255, '... ') !!}
						</p>
						<p>
							<a href="{{route('berita.isi',$hasil->slug)}}" class="btn btn-primary btn-sm">Read more</a>
						</p>
					</div>
					@endforeach
					<nav aria-label="Page navigation example">
						<div class="pagination justify-content-center mt-3">
						{{$data->links('pagination::bootstrap-4')}}
						</div>
					</nav>

					<!-- bottom -->
				</div>
			</section>
@endsection
		
