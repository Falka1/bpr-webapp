@extends('front.master')

@section('front')


			<!-- Carousel -->
			<div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="true">
				<div class="carousel-indicators">
					<button
						type="button"
						data-bs-target="#carouselExampleIndicators"
						data-bs-slide-to="0"
						class="active"
						aria-current="true"
						aria-label="Slide 1"
					></button>
					<button
						type="button"
						data-bs-target="#carouselExampleIndicators"
						data-bs-slide-to="1"
						aria-label="Slide 2"
					></button>
					<button
						type="button"
						data-bs-target="#carouselExampleIndicators"
						data-bs-slide-to="2"
						aria-label="Slide 3"
					></button>
				</div>
				<div class="carousel-inner bg-primary">
					<div class="carousel-item active">
						<img src="https://via.placeholder.com/500" class="d-block w-100" alt="..." />
					</div>
					<div class="carousel-item">
						<img src="https://via.placeholder.com/500" class="d-block w-100" alt="..." />
					</div>
					<div class="carousel-item">
						<img src="https://via.placeholder.com/500" class="d-block w-100" alt="..." />
					</div>
				</div>
				<button
					class="carousel-control-prev"
					type="button"
					data-bs-target="#carouselExampleIndicators"
					data-bs-slide="prev"
				>
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="visually-hidden">Previous</span>
				</button>
				<button
					class="carousel-control-next"
					type="button"
					data-bs-target="#carouselExampleIndicators"
					data-bs-slide="next"
				>
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="visually-hidden">Next</span>
				</button>
			</div>

			<!-- section about us -->
			<section class="text-center">
				<div class="about container">
					<h2>About Us</h2>
					<p class="lead">
					Merupakan salah satu Bank Perkreditan Rakyat (BPR) di Kota Bontang. PT. BPR Bontang Sejahtera melayani kegiatan usaha secara konvensional atau berdasarkan prinsip syariah yang dalam kegiatannya tidak memberikan jasa dalam lalu lintas pembayaran.

					beberapa layanan yang tersedia diantara lain. simpan deposito berjangka atau tabungan, kredit dan pinjaman, pembiayaan dan penempatan dana berdasarkan prinsip syariah.
					</p>
					<a href="#" class="btn btn-lg btn-primary">Button</a>
				</div>
			</section>

			<!-- section card -->
			<section>
				<div class="card-section mt-5">
					<div class="row text-center">
						<div class="col-md">
							<div class="card">
								<div class="wrap-card-img">
									<a href="">
										<img src="https://via.placeholder.com/500" class="card-img-top" alt="..." />
									</a>
								</div>
								<a href="" style="color: white">
									<div class="card-body">
										<h5 class="card-title">Tabungan dan Deposito</h5>
										<p class="card-text">
										Suku bunga tinggi, Biaya Murah, Flexible<br>
										Sambut Hari Depan Terencana
										</p>
									</div>
								</a>
							</div>
						</div>
						<div class="col-md">
							<div class="card">
								<div class="wrap-card-img">
									<a href="">
										<img src="https://via.placeholder.com/500" class="card-img-top" alt="..." />
									</a>
								</div>
								<a href="" style="color: white">
									<div class="card-body">
										<h5 class="card-title">Tabungan dan Deposito</h5>
										<p class="card-text">
										Suku bunga tinggi, Biaya Murah, Flexible<br>
										Sambut Hari Depan Terencana
										</p>
									</div>
								</a>
							</div>
						</div>
						<div class="col-md">
							<div class="card">
								<div class="wrap-card-img">
									<a href="">
										<img src="https://via.placeholder.com/500" class="card-img-top" alt="..." />
									</a>
								</div>
								<a href="" style="color: white">
									<div class="card-body">
										<h5 class="card-title">Tabungan dan Deposito</h5>
										<p class="card-text">
										Suku bunga tinggi, Biaya Murah, Flexible<br>
										Sambut Hari Depan Terencana
										</p>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</section>

			<!-- section 2 -->
			<section>
				<div class="container body-post">
					<div class="row">
						<div class="col-md">
							<img class="img-body w-100" src="https://via.placeholder.com/500" alt="" />
						</div>
						<div class="col-md mt-3 mt-md-0 text-center text-md-start">
							<h3>Lorem, ipsum dolor.</h3>
							<p class="lead">
								Lorem ipsum, dolor sit amet consectetur adipisicing elit. Maiores fuga perferendis eaque reiciendis
								quisquam dolore laborum ad magnam non quos?
							</p>
							<a href="#" class="btn btn-primary">Button</a>
						</div>
					</div>
				</div>
			</section>

			<!-- Location -->
			<section class="location text-center">
				<div class="wrapper container">
					<h2>Location</h2>
					<p class="lead">
					Jl. RE. Martadinata, Loktuan, No.7 RT.16, Bontang, Kalimantan Timur.<br> 
					Kode Pos 75321 <br>No. Telp(085282007009)
					</p>
					<a href="https://goo.gl/maps/PwoHt5hRJ9gH2trM9" target="_blank" class="btn btn-primary">Lokasi</a>
				</div>
			</section>

			<!-- article -->
			<section class="article">
				<div class="container wrapper">
					<div class="row gap-2">
						<!-- news -->
						<div class="col-lg">
							<h3>Berita Terkini</h3>
							<div class="mt-3"></div>

							@foreach($data as $result => $hasil)
							@if($result > 0)
							<div class="row mb-3 ">
								<div class="col-5">
									<div class="news">
										<a href="{{route('berita.isi',$hasil->slug)}}" >
											<img  src="{{$hasil -> gambar}}" alt="" />
										</a>
									</div>
								</div>
								<div class="col-7">	

									<h5>
										<a href="">
											{{ \Illuminate\Support\Str::limit($hasil -> judul,25, '... ') }}
										</a>
									</h5>
									<p class="text-xs text-danger">
									{{$hasil->category->name}}
									</p>
									<p class="text-xs text-secondary">
									{{date('d M y', strtotime($hasil->created_at))}}
									</p>
								</div>
							</div>
							@endif
							@endforeach
							
						</div>
						<!-- news one -->
						<div class="col-lg">
							<div class="news-one">
								<div class="wrap-img">
									<a href= "{{route('berita.isi',$data[0]->slug)}}">
									<img src="{{$data[0]['gambar']}}" alt="" />
									</a>

								</div>
								<h4>
									<a href="">
										{{$data[0]['judul']}}
									</a>
								</h4>
								<p class="text-xs text-danger">{!! date('d M y', strtotime($data[0]->created_at)) !!}</p>
								<p class="text-xs">
									{!! \Illuminate\Support\Str::limit($data[0]['content'], 225, '... ') !!}
									<a href="{{route('berita.isi',$data[0]->slug)}}" class="btn btn-primary btn-sm">Read more</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</section>

@endsection
