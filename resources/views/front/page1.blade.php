@extends('front.master')

@section('front')


			<section class="content1">
				<div class="container">
					<h2 class="text-primary mt-3">BERITA SEJAHTERA</h2>
					<!-- top news -->
					<div class="row">
						<div class="col-md-8">
							<div class="top_news">
							<a href="{{route('berita.isi',$data_top[0]->slug)}}" >
									<img src="{{$data_top[0]['gambar']}}" alt="" />
							</a>
								<div class="word">
									<h5 class="text-danger">{{$data_top[0]->category->name}}</h5>
									<h2 class="fw-bold text-white">{{$data_top[0]['judul']}}</h2>
									<p class="text-sm text-white">{{$data_top[0]->users->name}}, {{date('d M y', strtotime($data_top[0]->created_at))}}</p>
								</div>
							</div>
						</div>
						<div class="col-md-4 mt-3 mt-md-0">
							<div class="second_news">
								<div class="post">
								<a href="{{route('berita.isi',$data_top[1]->slug)}}" >
									<img src="{{$data_top[1]['gambar']}}" alt="" />
								</a>	
									<div class="word">
										<h6 class="text-danger">{{$data_top[1]->category->name}}</h6>
										<h5 class="fw-bold text-white">{{$data_top[1]['judul']}}</h4>
										<p class="text-white">{{$data_top[1]->users->name}}, {{date('d M y', strtotime($data_top[1]->created_at))}}</p>
									</div>
								</div>
								<div class="post">
								<a href="{{route('berita.isi',$data_top[2]->slug)}}" >
									<img src="{{$data_top[2]['gambar']}}" alt="" />
								</a>
									<div class="word">
										<h6 class="text-danger">{{$data_top[2]->category->name}}</h6>
										<h5 class="fw-bold text-white">{{$data_top[2]['judul']}}</h4>
										<p class="text-white">{{$data_top[2]->users->name}},  {{date('d M y', strtotime($data_top[2]->created_at))}}</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- news recent -->
					<div class="row mt-5">
						<!-- news -->
						<div class="row col-md-8 news_recent">
							<h3 class="text-primary">Berita Terkini</h3>
							@foreach($data as $result => $hasil)
						
							<div class="col-md-6">
								<div class="news2wrap">
									<a href="{{route('berita.isi',$hasil->slug)}}" >
										<img  src="{{$hasil -> gambar}}" alt="" />
									</a>
								</div>
									<h5>{{ \Illuminate\Support\Str::limit($hasil -> judul,25, '... ') }}</h5>
									<p>{!! \Illuminate\Support\Str::limit($hasil -> content,99, '... ') !!}</p>
									<p>
										<a href="{{route('berita.isi',$hasil->slug)}}" class="btn btn-primary btn-sm">Read more</a>
									</p>
								</div>
							</div>
							@endforeach
						</div>
						<!-- category -->
						<div class="col-md-4 category">
							<div class="d-flex justify-space-between align-items-center">
								<h3>Category</h3>
								<div class="line_break"></div>
							</div>
							@foreach($category_list as $hasil)
							<div class="list_category">
								<p><a href='{{route("berita.category", $hasil->slug)}}'>{{$hasil->name}}</a></p>
								<p>{{$hasil->posts-> count()}}</p>
							</div>
							@endforeach
						</div>
					</div>

					<!-- pagination -->
					<nav aria-label="Page navigation example">
						<div class="pagination justify-content-center mt-3">
						{{$data->links('pagination::bootstrap-4')}}
						</div>
					</nav>

					<!-- bottom -->
				</div>
			</section>

@endsection
