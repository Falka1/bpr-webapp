<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<title>Bootstrap demo</title>
		<!-- Bootstrap -->
		<link
			href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css"
			rel="stylesheet"
			integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor"
			crossorigin="anonymous"
		/>
		<!-- Font -->
		<link
			href="https://fonts.googleapis.com/css2?family=Plus+Jakarta+Sans:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;1,200;1,300;1,400;1,500;1,600;1,700;1,800&display=swap"
			rel="stylesheet"
		/>
		<!-- Css Custom -->
		<link rel="stylesheet" href="{{asset('front/styles.css')}}" />
		<link rel="stylesheet" href="{{asset('front/page1.css')}}" />
		<!-- Font Awesome CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
	</head>
	<body>
		<div class="bpr">
			<a href="#" class="chat">
				<p class="fw-bold">Hubungi Kami... </p>
				<img class="icon" src="https://seeklogo.com/images/W/whatsapp-icon-logo-6E793ACECD-seeklogo.com.png" alt="" />
			</a>
			<!-- Navbar Head -->
			<div class="navbar bg-light">
				<div class="container">
					<a class="navbar-brand" href="/">
						<img src="{{asset('public/front/Logo BPR1.png')}}" alt="" height="50" />
					</a>
					<form class="d-sm-flex d-none" role="search" action="{{route('berita.search')}}" >
						<input class="form-control me-2" name='cari' type="search" placeholder="Search" aria-label="Search" />
						<button class="btn btn-outline-success" type="submit">Search</button>
					</form>
				</div>
			</div>

			<!-- Navbar Menu -->
			<div class="navbar sticky-top navbar-expand-sm navbar-dark bg-primary nav-cust nav-menu">
				<button class="navbar-toggler ms-auto" type="button" data-bs-toggle="collapse" data-bs-target="#navmenu">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse justify-content-center" id="navmenu">
					<ul class="navbar-nav">
						<div class="input-group mt-3 mb-3 px-3 d-sm-none">
							<input type="text" class="form-control" placeholder="Search" />
							<button class="btn btn-secondary" type="button">Search</button>
						</div>
						<li class="nav-item">
							<a href="/" class="nav-link link">Home</a>
						</li>
						<li class="nav-item">
							<a href="#question" class="nav-link link">Question</a>
						</li>
						<li class="nav-item">
							<a href="#instructor" class="nav-link link">Instructor</a>
						</li>
						<li class="nav-item">
							<a href="{{route('berita.utama')}}" class="nav-link link">Berita Terkini</a>
						</li>
					</ul>
				</div>
			</div>

            @yield('front')


			<!-- footer -->
			<footer>
				<div class="container">
					<div class="line-footer"></div>
					<h4>PT BPR Bontang Sejahtera</h4>
				</div>
			</footer>

			<!-- END -->
		</div>

		<script
			src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"
			integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2"
			crossorigin="anonymous"
		></script>
	</body>
</html>
