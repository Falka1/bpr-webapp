@extends('front.master')

@section('front')

@foreach($data as $isi)
			<section class="content1">
				<div class="container">
					<h2 class="text-primary mt-3">BERITA SEJAHTERA</h2>
					<h1 class="fw-bold">{{$isi -> judul}}</h1>
					<div class="d-flex gap-2">
						<h5 class="text-danger">{{$isi -> category->name}}</h5>
						<p class="text-sm text-secondary">{{date('d M y', strtotime($isi->created_at))}}</p>
					</div>
					<div class="row">
						<div class="col-md-8 news_detail">
							<img src="{{asset($isi->gambar)}}" alt="" />
							<p class="mt-4">
								{!! $isi -> content !!}
							</p>
						</div>
@endforeach
						<div class="col-md-4 category sticky">
							<div class="d-flex justify-space-between align-items-center">
								<h3>Category</h3>
								<div class="line_break"></div>
							</div>
							@foreach($category_list as $hasil)
							<div class="list_category">
								<p><a href='{{route("berita.category", $hasil->slug)}}'>{{$hasil->name}}</a></p>
								<p>{{$hasil->posts-> count()}}</p>
							</div>
							@endforeach
						</div>
					</div>
				</div>
				<!-- news recent -->
				<div class="news_recent2 mt-3">
					<div class="container pt-2">
						<h3 class="text-primary mt-4">Berita Terkini</h3>
						<div class="row">
							<div class="col-md-4 news_recent_ref">
								<img src="https://via.placeholder.com/500" alt="" />
								<h5 class="mt-2">Lorem, ipsum dolor.</h5>
								<p>
									Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt, rem. Odit minima quia corrupti hic
									quod nisi nulla, rerum asperiores?
								</p>
							</div>
							<div class="col-md-4 news_recent_ref">
									<img src="https://via.placeholder.com/500" alt="" />
									<h5 class="mt-2">Lorem, ipsum dolor.</h5>
									<p>
										Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt, rem. Odit minima quia corrupti hic
										quod nisi nulla, rerum asperiores?
									</p>
							</div>
							<div class="col-md-4 news_recent_ref">
								<img src="https://via.placeholder.com/500" alt="" />
								<h5 class="mt-2">Lorem, ipsum dolor.</h5>
								<p>
									Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt, rem. Odit minima quia corrupti hic
									quod nisi nulla, rerum asperiores?
								</p>
							</div>
						</div>
					</div>
				</div>

				<!-- bottom -->
			</section>

@endsection
