<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\KaryawanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['auth','CheckRole:1']], function(){
    Route::get('/post/trashed',[PostController::class, 'trashed'])->name('post.trashed');
    Route::get('/post/restore/{id}',[PostController::class, 'restore'])->name('post.restore');
    Route::delete('/post/kill/{id}',[PostController::class, 'kill'])->name('post.kill');
    

    Route::resource('user', UserController::class);
    Route::resource('karyawan', KaryawanController::class);
    Route::post('/karyawan/images',[KaryawanController::class, 'image_upload'])->name('karyawan.image_upload');

});

Route::group(['middleware' => ['auth','CheckRole:1,2,3']], function(){
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::resources([
        'post' => PostController::class,
        'tag' => TagController::class,
        'category' => CategoryController::class,
    ]);
});


Route::get('/',[BlogController::class, 'index']);
Route::get('/berita-sejahtera',[BlogController::class, 'berita'])->name('berita.utama') ;
Route::get('/berita-sejahtera/list/{category}',[BlogController::class, 'category_berita'])->name('berita.category');
Route::get('/berita-sejahtera/search',[BlogController::class, 'search_berita'])->name('berita.search');
Route::get('/berita-sejahtera/{slug}',[BlogController::class, 'isi_berita'])->name('berita.isi');









